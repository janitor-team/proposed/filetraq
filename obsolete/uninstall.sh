#!/bin/sh

# uninstall script for FileTraq v0.2
# Copyright (c) 2000 Jeremy Weatherford
# See COPYING file included with distribution

if [ ! -e .bindir ]; then
    echo FileTraq 0.2 has not been installed, or was not installed from this directory.
    exit
fi

if [ ! -e .datadir ]; then
    echo FileTraq 0.2 has not been installed, or was not installed from this directory.
    exit
fi

bindir=`cat .bindir`
datadir=`cat .datadir`

echo "This program will uninstall FileTraq v0.2."
echo "The following commands will be executed:"
echo "  rm -f $bindir/filetraq"
echo "  rm -fr $datadir"
echo "Do you wish to continue uninstalling FileTraq v0.2?  [yes/NO] "
read resp
case $resp in
    y*|Y*)
	rm -f $bindir/filetraq
	rm -fr $datadir
	echo "Uninstallation complete."
	echo "If you added a crontab entry for FileTraq, you need to remove that entry."
	rm -f .bindir
	rm -f .datadir
	exit
esac

echo "Uninstallation canceled."
#!/bin/sh

# install script for FileTraq v0.2
# Copyright (c) 2000 Jeremy Weatherford
# See COPYING file included with distribution

echo This script will install FileTraq 0.2 in the directories you specify.
echo If you are not root, hit control-C now.
echo
echo "If you're presently using FileTraq 0.1, simply specify the same"
echo "directories, and FileTraq 0.2 will be installed on top of the"
echo "existing installation."
echo

ok=0
while [ $ok = 0 ]; do
    echo -n "Where would you like the FileTraq binary?  [/usr/local/sbin] "
    read dir
    if [ -z "$dir" ]; then
	dir=/usr/local/sbin
    fi
    if [ ! -d $dir ]; then
	echo
	echo -n "$dir does not exist.  Create it now?  [yes/NO] "
	read resp
	echo
	case $resp in
	    y*|Y*)
		mkdir $dir
		ok=1
		;;
	esac
    else
	ok=1
    fi
done

bindir=$dir
echo Using binary directory $bindir.
echo

ok=0
while [ $ok = 0 ]; do
    echo -n "Where would you like FileTraq to keep its files?  [/usr/local/filetraq] "
    read dir
    if [ -z "$dir" ]; then
	dir=/usr/local/filetraq
    fi
    if [ ! -d $dir ]; then
	echo
	echo -n "$dir does not exist.  Create it now?  [yes/NO] "
	read resp
	echo
	case $resp in
	    y*|Y*)
		mkdir $dir
		ok=1
		;;
	esac
    else
	ok=1
    fi
done

datadir=$dir
echo Using data directory $datadir.
echo

echo "Patching shell script for default values and copying to $bindir..."
cat filetraq | sed "s|/usr/local/filetraq|$dir|g" > $bindir/filetraq
chmod 700 $bindir/filetraq
echo "Setting up data directory in $datadir..."
if [ -e $datadir/filelist ]; then
    mv -f $datadir/filelist $datadir/filetraq.conf
    echo "Renamed $datadir/filelist to $datadir/filetraq.conf..."
fi

if [ ! -e $datadir/filetraq.conf ]; then
    cat filetraq.conf | sed "s|/usr/local/filetraq|$dir|g" > $datadir/filetraq.conf
    echo "Setting up default filetraq.conf in $datadir..."
else
    echo "Continuing to use existing filetraq.conf in $datadir..."
fi

if [ ! -d $datadir/backups ]; then
    mkdir $datadir/backups
    chmod 700 $datadir
    chmod 700 $datadir/backups
fi


echo $bindir > .bindir
echo $datadir > .datadir

echo
echo All done!
echo "FileTraq is designed to be run at intervals from root's crontab, with its output being mailed to root.  The files checked are controlled by $datadir/filelist.  Please see the README file for more information.  Thank you!"

